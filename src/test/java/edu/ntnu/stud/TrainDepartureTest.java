package edu.ntnu.stud;


import java.time.LocalTime;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for the TrainDeparture class.
 *
 * <p>This class contains tests for the constructor of the TrainDeparture class,
 * including positive and negative scenarios to ensure proper object creation and handling
 * of invalid values.</p>
 */
public class TrainDepartureTest {

  @Test
  public void testConstructor() {
    LocalTime departureTime = LocalTime.of(8, 30);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, "L1", "63", "Oslo", 1, LocalTime.of(0, 0));

    // Positive test: Checks that the object is crated as expected
    assertEquals(departureTime, trainDeparture.getDepartureTime());
    assertEquals("L1", trainDeparture.getLine());
    assertEquals("63", trainDeparture.getTrainNumber());
    assertEquals("Oslo", trainDeparture.getDestination());
    assertEquals(1, trainDeparture.getTrack());
    assertEquals(LocalTime.of(0, 0), trainDeparture.getDelay());
  }

  /**
   * Negative test scenarios for the TrainDeparture constructor.
   *
   * <p>Tries to create an object with invalid values and expects IllegalArgumentException.</p>
   */
  @Test
  public void testNegativeScenario() {
    // Negative test: Tries to create an object with invalid values
    assertThrows(IllegalArgumentException.class, () ->
            new TrainDeparture(null, "L1", "63", "Oslo", 1, LocalTime.of(0, 0))
    );

    assertThrows(IllegalArgumentException.class, () ->
            new TrainDeparture(LocalTime.of(8, 30), null, "63", "Oslo", 1, LocalTime.of(0, 0))
    );

    assertThrows(IllegalArgumentException.class, () ->
            new TrainDeparture(LocalTime.of(8, 30), "L1", null, "Oslo", 1, LocalTime.of(0, 0))
    );

    assertThrows(IllegalArgumentException.class, () ->
            new TrainDeparture(LocalTime.of(8, 30), "L1", "63", null, 1, LocalTime.of(0, 0))
    );

    assertThrows(IllegalArgumentException.class, () ->
            new TrainDeparture(LocalTime.of(8, 30), "L1", "63", "Oslo", -1, LocalTime.of(0, 0))
    );
  }
}
