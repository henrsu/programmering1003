package edu.ntnu.stud;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;

/**
 * Class for the train departure register testing.
 *
 * @author Henrik Sund
 * @version 2.0 2023-11-20
 */
class TrainDepartureRegisterTest {

  @Test
  void testRegisterTrainDeparture() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    TrainDeparture departure = new TrainDeparture(LocalTime.of(9, 0), "L1", "123", "City", 2, LocalTime.of(0, 0));

    // Positive test: Register a valid train departure
    register.registerTrainDeparture(departure);
    assertTrue(register.trainDepartureList.contains(departure));
  }

  @Test
  void testCreateTestData() {
    TrainDepartureRegister register = new TrainDepartureRegister();

    // Positive test: Create test data
    register.createTestData();
    assertFalse(register.trainDepartureList.isEmpty());
  }

  @Test
  void testUpdateDepartureTime() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    register.createTestData();

    // Positive test: Update departure time and check for filtered departures
    register.updateDepartureTime(9, 0);
    assertFalse(register.trainDepartureList.isEmpty());

    // Negative test: Attempt to update with invalid time (hours < 0)
    assertThrows(IllegalArgumentException.class, () -> register.updateDepartureTime(-1, 0));
  }

  @Test
  void testFindDepartureByTrainNumber() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    register.createTestData();

    // Positive test: Find departure by valid train number
    assertDoesNotThrow(() -> register.findDepartureByTrainNumber("63"));

    // Negative test: Attempt to find departure with invalid train number
    assertThrows(IllegalArgumentException.class, () -> register.findDepartureByTrainNumber("InvalidNumber"));
  }

  @Test
  void testFindDepartureByDestination() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    register.createTestData();

    // Positive test: Find departure by valid destination
    assertDoesNotThrow(() -> register.findDepartureByDestination("Oslo"));

    // Negative test: Attempt to find departure with invalid destination
    assertThrows(IllegalArgumentException.class, () -> register.findDepartureByDestination("InvalidDestination"));
  }

  @Test
  void testPrintDeparturesSorted() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    register.createTestData();

    // Positive test: Print sorted departures
    assertDoesNotThrow(register::printDeparturesSorted);
  }
}
