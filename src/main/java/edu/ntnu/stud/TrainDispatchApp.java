package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * The main class for the train dispatch application.
 * This class contains the main method to start the application.
 * Usage:
 * - Instantiate objects and interact with the train departure system.
 *
 * @author Henrik Sund
 * @version 2.0 2023-11-20
 */
public class TrainDispatchApp {
  /**
   * The main entry point for the train dispatch application.
   * Creates an instance of TrainDeparture and demonstrates its usage.
   *
   */

  public static void main(String[] args) {
    // Create a TrainDeparture instance for demonstration
    Interface menuInterface = new Interface();
    menuInterface.init();
    menuInterface.start();
  }
}
