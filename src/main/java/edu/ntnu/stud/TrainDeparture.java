package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;

/**
 * A class that represent a train departure.
 *
 * @version 2.0 2023-11-20
 * @author Henrik Sund
 */

public class TrainDeparture {

  private LocalTime departureTime;
  private String line;
  private String trainNumber;
  private String destination;
  private int track;
  private LocalTime delay;
  private LocalTime totalTime;

  /**
   *  Constructor.
   *
   * @param line Defines a distance the train drive
   * @param trainNumber Unique number for the train
   * @param destination End destination for the train
   * @param track The Track number
   */

  public TrainDeparture(LocalTime departureTime, String line, String trainNumber,
                        String destination, int track, LocalTime delay) {
    if (departureTime == null || line == null || trainNumber == null || destination == null || track < 0) {
      throw new IllegalArgumentException("Invalid arguments for TrainDeparture");
    }

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.delay = delay;
    this.totalTime = departureTime.plusHours(delay.getHour())
            .plusMinutes(delay.getMinute());


  }
  //public TrainDeparture() {}

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  public String getLine() {
    return line;
  }

  public String getTrainNumber()  {
    return trainNumber;
  }

  public String getDestination() {
    return destination;
  }

  public int getTrack() {
    return track;
  }

  public LocalTime getDelay() {
    return delay;
  }

  public LocalTime getTotalTime() {
    return totalTime;
  }

  @Override
  public String toString() {
    return "Departure{"
            + "Departure time= " + departureTime
            + ", line='" + line + '\''
            + ", train number='" + trainNumber + '\''
            + ", destination='" + destination + '\''
            + ", delay='" + delay + '\''
            + ", track=" + track
            + '}';
  }


}
