package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Class for the Interface.
 *
 * @author Henrik Sund
 * @version 2.0 2023-11-20
 */
public class Interface {
  public Interface() {}

  TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();

  /**
   *Method that initiate the program.
   *  Prepares the project
   */
  public void init() {

  }

  /**
   * Starts the program.
   * Performs the main tasks of the program.
   * Adds some example train departures and prints them.
   */

  public void start() {
    System.out.println();
    trainDepartureRegister.createTestData();
    trainDepartureRegister.printDepartures();
    trainDepartureRegister.findDepartureByTrainNumber("63");
    trainDepartureRegister.findDepartureByDestination("Oslo");
    trainDepartureRegister.updateDepartureTime(11, 31);
    trainDepartureRegister.printDeparturesSorted();
  }


}
