package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Class for the train departure register.
 *
 * @author Henrik Sund
 * @version 2.0 2023-11-20
 */
public class TrainDepartureRegister {
  private final Logger LOGGER = Logger.getLogger(TrainDepartureRegister.class.getName());
  //private LocalTime currentTime;
  ArrayList<TrainDeparture> trainDepartureList = new ArrayList<>();

  public TrainDepartureRegister() {}

  LocalTime currentTime = LocalTime.of(7, 0);

  /**
   * Validates whether a train with a specific number already exist in the trainDepartureList.
   *
   * @param trainNumber Number of the train to be validated.
   * @return Returns true if the number is unique, otherwise, returns false
   */
  public boolean validateData(String trainNumber) {
    boolean isDuplicate = trainDepartureList.stream()
            .anyMatch(train -> train.getTrainNumber().equals(trainNumber));

    if (isDuplicate) {
      LOGGER.info("Train departure with the same train number already exists.");
      return false;  // or throw an exception, depending on your use case
    }
    return true;  // Indicate that validation succeeded, and the train departure can be added
  }

  /**
   * Registers a train departure by adding it to the list if the provided train number is valid.
   *
   * @param trainDeparture The TrainDeparture object to be registered.
   * @see #validateData(String) to ensure the train number is unique before registration.
   */
  public void registerTrainDeparture(TrainDeparture trainDeparture) {
    if (validateData(trainDeparture.getTrainNumber())) {
      trainDepartureList.add(trainDeparture);
    }
  }

  /**
   * Creates and registers test train departures for demonstration purposes.
   *
   * <p>The method registers several TrainDeparture instances with different parameters
   * to simulate a variety of test scenarios.</p>
   *
   * <p>Use this method to populate the train departure list with sample data for testing and development.</p>
   */
  public void createTestData() {
    registerTrainDeparture(new TrainDeparture(LocalTime.of(8, 30), "L1", "63", "Oslo", 1, LocalTime.of(2, 30)));
    registerTrainDeparture(new TrainDeparture(LocalTime.of(19, 30), "L1", "65", "Bodø", 1, LocalTime.of(0, 3)));
    registerTrainDeparture(new TrainDeparture(LocalTime.of(10, 30), "L1", "63", "Oslo", 1, LocalTime.of(0, 55)));
    registerTrainDeparture(new TrainDeparture(LocalTime.of(7, 30), "L1", "64", "Oslo", 1, LocalTime.of(3, 30)));
    registerTrainDeparture(new TrainDeparture(LocalTime.of(11, 30), "L1", "66", "Oslo", 1, LocalTime.of(1, 15)));
  }

  public void printDepartures() {
    trainDepartureList
            .forEach(System.out::println);
  }

  /**
   * Finds and prints train departures based on the provided filter and label.
   *
   * <p>This method prints train departures from the specified label by applying the given
   * filter using a {@link java.util.function.Predicate}.</p>
   *
   * @param filter The predicate used to filter train departures.
   * @param label  The label indicating the source or destination of the train departures.
   *               This label is printed before displaying the matching departures.
   */
  public void findDeparture(Predicate<TrainDeparture> filter, String label) {
    System.out.println();
    System.out.println("From " + label); // Remove later
    trainDepartureList.stream()
            .filter(filter)
            .forEach(System.out::println);
  }

  public void findDepartureByTrainNumber(String trainNumber) {
    findDeparture(train -> train.getTrainNumber().equals(trainNumber), "Train number");
  }

  public void findDepartureByDestination(String destination) {
    findDeparture(train -> train.getDestination().equals(destination), "Destination");
  }

  /**
   * Updates the current time and filters train departures based on the new time.
   *
   * <p>This method sets the current time to the specified hours and minutes, filters
   * the train departures list to include only those with departure times after the updated
   * current time, and prints the filtered departures along with the current time.</p>
   *
   * @param hours   The new hours for the current time.
   * @param minutes The new minutes for the current time.
   */
  public void updateDepartureTime(int hours, int minutes) {
    currentTime = LocalTime.of(hours, minutes);
    System.out.println();
    List<TrainDeparture> filteredDepartures = trainDepartureList.stream()
                    .filter(train -> train.getTotalTime().isAfter(currentTime))
                    .collect(Collectors.toList());
    System.out.println("Filtered by time");
    filteredDepartures.forEach(System.out::println);
    trainDepartureList = (ArrayList<TrainDeparture>) filteredDepartures;
    System.out.println();
    System.out.println("Current time: " + currentTime);

  }

  /**
   * Prints the list of train departures in sorted order based on departure time.
   *
   * <p>This method sorts the train departures by their departure time in ascending order
   * and prints the sorted list of departures.</p>
   */
  public void printDeparturesSorted() {
    System.out.println();
    System.out.println("SortedList");
    trainDepartureList.stream()
            .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
            .forEach(System.out::println);
  }
}
